<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 12/12/14
 * Time: 08:49
 */

namespace conf;

use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class ConfigEloquent : configuration de Eloquent pour le lien avec la base de donnees
 * @package conf
 */
class ConfigEloquent {

    /**
     * methode qui charge le fichier de configuration de la base de donnees
     * puis creer la connection avec la base de donnees
     */
    public static function configurer(){
        $db = new DB();
        $ini = parse_ini_file("db.conf.ini");
        $db->addConnection($ini);

        $db->setAsGlobal();
        $db->bootEloquent();
    }
} 