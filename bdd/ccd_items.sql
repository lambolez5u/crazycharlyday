-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 05 Mars 2015 à 16:54
-- Version du serveur: 5.5.41-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `crazycharlyday`
--

-- --------------------------------------------------------

--
-- Structure de la table `ccd_items`
--

CREATE TABLE IF NOT EXISTS `ccd_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `piece_id` int(11) NOT NULL,
  `photo` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `couleur` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `prix` float NOT NULL,
  `type_id` int(11) NOT NULL,
  `promo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Contenu de la table `ccd_items`
--

INSERT INTO `ccd_items` (`id`, `nom`, `description`, `piece_id`, `photo`, `couleur`, `prix`, `type_id`, `promo`) VALUES
(1, 'Fauteuil gonflable ', '', 1, 'fauteuil_gonflable.jpg', 'Mauve', 150, 4, 0),
(2, 'Sofa Gonflable', '', 1, 'sofa_gonflable.jpg', 'Orange', 175.99, 4, 0),
(3, 'Fauteuil en carton', '', 1, 'fauteuil.jpg', 'Maron', 15.95, 1, 0),
(4, 'Vache', '', 2, 'vache_blanche.jpg', 'Blanche', 9.99, 1, 0),
(5, 'Fauteuil en palettes', '', 2, 'meuble_palette.jpg', 'Marron', 230.9, 2, 0),
(6, 'Tortue', '', 3, 'tortue_rouge.jpg', 'Rouge', 25.85, 1, 0),
(7, 'Tortue', '', 3, 'tortue_brun.jpg', 'Brun', 25.85, 1, 0),
(8, 'Canard Gonflable', '', 4, 'canard_gonflable.jpg', 'Jaune', 4.99, 4, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
