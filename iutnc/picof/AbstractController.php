<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 19/11/14
 * Time: 09:01
 */

namespace iutnc\picof;


use iutnc\picof\utils\HttpRequest;

/**
 * Class AbstractController
 * @package iutnc_madert2u\picof
 */
abstract class AbstractController {

    protected $request;

    /**
     * constructeur de tout les controleurs
     * @param HttpRequest $request
     */
    public function __construct(HttpRequest $request){
        $this->request = $request;
    }

} 