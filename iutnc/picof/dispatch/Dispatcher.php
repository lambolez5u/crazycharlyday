<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 19/11/14
 * Time: 09:12
 */

namespace iutnc\picof\dispatch;

use iutnc\appcatalogue\control\CatalogueController;

/**
 * Class Dispatcher
 * @package iutnc_madert2u\picof\dispatch
 */
class Dispatcher {

    private $req, $route;

    /**
     * constructeur de Dispatcher
     * @param $request
     */
    public function __construct($request){
        $this->req = $request;
        $this->route = array();
    }

    /**
     * methode qui permet l'ajout de nouvelles routes (lien entre l'url, le controlleur et la methode du controlleur
     * @param $url
     * @param $controller
     * @param $method
     */
    public function addRoute($url, $controller, $method){
        $this->route[$url] = array('c'=>$controller, 'm'=>$method);
    }

    /**
     * methode qui recupere la commande de l'url, creer le controlleur et lance la bonne methode du controlleur
     */
    public function dispatch(){
        if (array_key_exists($this->req->getPathInfo(), $this->route)){
            $controller = $this->route[$this->req->getPathInfo()]['c'];
            $methode = $this->route[$this->req->getPathInfo()]['m'];
        } else {
            $controller = $this->route["/"]['c'];
            $methode = $this->route["/"]['m'];
        }
        $c = new $controller($this->req);
        $c->$methode();
    }
}