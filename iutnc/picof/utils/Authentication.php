<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 13/01/15
 * Time: 11:41
 */

namespace iutnc\picof\utils;


use iutnc\appcatalogue\models\User;
use iutnc\picof\AuthException;

//use PasswordPolicy\Policy;

/**
 * Class Authentication
 * @package iutnc_madert2u\picof\utils
 */
class Authentication {

    /**
     * fonction permettant la creation d'un nouvel utilisateur dans la base de donnees
     * @param $userName : nom du nouvel utilisateur
     * @param $password : mot de passe du nouvel utilisateur
     */
    public static function createUser($userName, $password, $d){
        // verifier la conformite du password avec la police
        /*$policy = new Policy();
        $policy->contains('lowercase', $policy->atLeast(2));
        $policy->length(6);
        $js = $policy->toJavaScript();
        echo "var policy = $js";
        $result = $policy->check($password);*/
        // si ok : hacher le password

        if (strlen($password)<6){
            throw new AuthException("le mot de passe doit contenir au moins 6 caractères");
        }

        $hash = password_hash($password, PASSWORD_DEFAULT, array('cost'=>12));
        //creer et enregistrer l'utilisateur
        $user = new User();
        $user->name = $userName;
        $user->password = $hash;
        $user->droit = $d;
        $user->save();
        self::loadProfile($user->id);
    }

    /**
     * fonction qui permet l'authentification d'un utilisateur
     * @param $userName
     * @param $password
     * @throws AuthException
     */
    public static function authenticate($userName, $password){
        // charger utilisateur $user
        $temp = User::where("name", "like", $userName)->get();
        $user = null;
        foreach ($temp as $u){
            $user = $u;
        }
        if ($user != null){
            $hash = $user->password;
            // vérifier le mot de passe
            if (password_verify($password, $hash)){
                // charger le profil
                self::loadProfile($user->id);
            } else {
                throw new AuthException("mauvais mot de passe");
            }
        } else {
            throw new AuthException("identifiant incorrect");
        }


    }

    /**
     * methode qui charge un utilisateur dans la session
     * @param $uid
     */
    public static function loadProfile($uid){
        //charger l'utilisateur et ses droits
        $user = User::find($uid);
        // créer variable de session = profil chargé
        $_SESSION["user"] = array( 'username'=>$user->name,
                                        'userid' => $user->id,
                                        'droit' => $user->droit);
    }

    /**
     * methode qui verifie les droits d'acces de l'utilisateur
     * @param $required
     * @throws AuthException
     */
    public static function checkAccessRight($required) {
        if (isset ($_SESSION["user"])){
            if ($_SESSION["user"]["droit"]<$required){
                throw new AuthException("vous n'avez pas les droit requis");
            }
        } else {
            throw new AuthException("vous devez vous connecter pour acceder à cette page");
        }
    }


}