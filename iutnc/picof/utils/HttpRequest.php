<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 19/11/14
 * Time: 08:22
 */

namespace iutnc\picof\utils;

/**
 * Class HttpRequest : permet de controller les variables du serveur (et de les simuler pour les tests)
 * @package iutnc_madert2u\picof\utils
 */
class HttpRequest {

    private $method, $script_name, $request_uri, $query, $get, $post, $session;

    /**
     * constructeur de HttpRequest, fait le lien entre les variables du serveur et celle de la class
     */
    public function __construct(){
        $this->script_name = $_SERVER['SCRIPT_NAME'];
        $this->request_uri = $_SERVER['REQUEST_URI'];
        $this->get = $_GET;
        $this->post = $_POST;
        $this->session = $_SESSION;
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->query = $_SERVER['QUERY_STRING'];
    }

    /**
     * methode qui retourne la commande de l'url
     * @return array|mixed|string
     */
    public function getPathInfo(){
        $s= dirname($this->script_name);
        $s = str_replace($s, "", $this->request_uri);
        $s = explode('?', $s);
        $s = $s[0];
        return $s;
    }

    /**
     * getter magique
     * @param $attname
     * @return mixed
     * @throws \Exception
     */
    public function __get($attname){
        if(property_exists($this, $attname)){
            return $this->$attname;
        } else {
            throw new \Exception("invalid property");
        }
    }

    /**
     * setter magique
     * @param $attname
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    public function __set($attname, $value){
        if(property_exists($this, $attname)){
            $this->$attname = $value;
            return $this->$attname;
        } else {
            throw new \Exception("invalid property");
        }
    }

} 