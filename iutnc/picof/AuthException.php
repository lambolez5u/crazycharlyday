<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 16/01/15
 * Time: 09:37
 */

namespace iutnc\picof;

/**
 * Class AuthException : exception concernant toutes les procedures en rapport avec l'authentification
 * @package iutnc_madert2u\picof
 */
class AuthException extends \Exception{
    public function __construct($m){
        parent::__construct($m);
    }
}