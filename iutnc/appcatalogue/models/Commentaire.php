<?php

namespace iutnc\appcatalogue\models;

/**
 * Class Item : fait le lien avec la table ccd_items de la base de donnees
 * @package iutnc_madert2u\blogapp\models
 */
class Commentaire extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "ccd_commentaires";
    protected $primaryKey = "id";
    public $timestamps = false;

    /**
     * methode qui assure la contrainte de cle etrangere avec la table categorie
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('iutnc\appcatalogue\models\User', 'user_id');
    }

    public function piece(){
        return $this->belongsTo('iutnc\appcatalogue\models\Item', 'item_id');
    }
}