<?php

namespace iutnc\appcatalogue\models;

/**
 * Class Item : fait le lien avec la table ccd_items de la base de donnees
 * @package iutnc_madert2u\blogapp\models
 */
class Item extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "ccd_items";
    protected $primaryKey = "id";
    public $timestamps = false;

    /**
     * methode qui assure la contrainte de cle etrangere avec la table categorie
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('iutnc\appcatalogue\models\Type', 'type_id');
    }

    public function piece(){
        return $this->belongsTo('iutnc\appcatalogue\models\Piece', 'piece_id');
    }

    public function commentaires(){
        return $this->hasMany('iutnc\appcatalogue\models\Commentaire', 'item_id');
    }

    public function plusUn(){
        $item = Item::find($this->id);
        $item->jaime += 1;
        $item->save();
    }

    public function moinsUn(){
        $item = Item::find($this->id);
        if($item->jaime > 0){
            $item->jaime += 1;
            $item->save();
        }
    }
}