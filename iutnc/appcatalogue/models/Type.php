<?php

namespace iutnc\appcatalogue\models;

/**
 * Class Type : lien avec la table ccd_types de la base de donnees
 * @package iutnc_madert2u\blogapp\models
 */
class Type extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'ccd_types';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * methode qui assure la contrainte de cle etrangere avec la table ccd_items
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items(){
        return $this->hasMany('iutnc\appcatalogue\models\Item', 'type_id');
    }
} 