<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 14/01/15
 * Time: 16:29
 */

namespace iutnc\appcatalogue\models;


/**
 * Class User : lien avec la table user de la base de donnees
 * @package iutnc_madert2u\blogapp\models
 */
class User extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'ccd_users';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function commentaires(){
        return $this->hasMany('iutnc\appcatalogue\models\Commentaire', 'user_id');
    }
}