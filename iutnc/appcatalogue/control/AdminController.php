<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 19/11/14
 * Time: 09:04
 */

namespace iutnc\appcatalogue\control;


use Illuminate\Database\QueryException;
use iutnc\appcatalogue\models\Item;
use iutnc\appcatalogue\models\Type;
use iutnc\appcatalogue\models\Commentaire;
use iutnc\appcatalogue\models\Piece;
use iutnc\appcatalogue\vue\VueAdmin;
use iutnc\appcatalogue\vue\VueCatalogue;
use iutnc\picof\AbstractController;
use iutnc\picof\AuthException;
use iutnc\picof\utils\Authentication;

/**
 * Class AdminController
 * @package iutnc_madert2u\blogapp\control
 */
class AdminController extends AbstractController{

    /**
     * methode qui creer la vue pour ajouter un item
     */
    public function addItem(){
        try {
            Authentication::checkAccessRight(2);
            $v = new VueAdmin($this->request);
            $v->render(1);
        } catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, $e->getMessage());
        }

    }

    /**
     * methode qui enregistre un billet dans la base de donnees
     */
    public function saveItem(){
        try {
            Authentication::checkAccessRight(2);
            if (isset($this->request->post["nom"])  && isset($this->request->post["type"]) && isset($this->request->post["piece"]) && isset($this->request->post["prix"]) && isset($this->request->post["couleur"])) {
                $b = new Item();
                $b->nom = htmlspecialchars($this->request->post["nom"]);
                $b->description = htmlspecialchars($this->request->post["description"]);
                $b->type_id = $this->request->post["type"];
				$b->photo="indisponible.jpg";
                $b->piece_id = $this->request->post["piece"];
                $b->couleur = htmlentities($this->request->post["couleur"]);
                $b->prix = htmlspecialchars($this->request->post["prix"]);
                $b->save();
                $v = new VueCatalogue($b, $this->request);
                $v->render(1);
            } else {
                self::addItem();
            }
        } catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, $e->getMessage());
        }
    }

    /**
     * methode qui creer la vue pour ajouter une categorie
     */
    public function addType(){
        try {
            Authentication::checkAccessRight(2);
            $v = new VueAdmin($this->request);
            $v->render(2);
        }catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, $e->getMessage());
        }
    }

    /**
     * methode qui enregistre dans la base de donnees la nouvelle categorie
     */
    public function saveType(){
        try {
            Authentication::checkAccessRight(2);
            if (isset($this->request->post["nom"])) {
                $c = new Type();
                $c->type = htmlspecialchars($this->request->post["nom"]);
                $c->save();
                $v = new VueAdmin($this->request);
                $v->render(1);
            } else {
                self::addType();
            }
        } catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, $e->getMessage());
        }
    }

    /**
     * methode qui creer la vue pour ajouter une categorie
     */
    public function addPiece(){
        try {
            Authentication::checkAccessRight(2);
            $v = new VueAdmin($this->request);
            $v->render(5);
        }catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, $e->getMessage());
        }
    }

    /**
     * methode qui enregistre dans la base de donnees la nouvelle categorie
     */
    public function savePiece(){
        try {
            Authentication::checkAccessRight(2);
            if (isset($this->request->post["nom"])) {
                $c = new Piece();
                $c->nom = htmlspecialchars($this->request->post["nom"]);
                $c->description = htmlspecialchars($this->request->post["description"]);
                $c->save();
                $v = new VueAdmin($this->request);
                $v->render(1);
            } else {
                self::addType();
            }
        } catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, $e->getMessage());
        }
    }

    public function affSetPromo()
    {
        $v = new VueAdmin($this->request);
        $v->render(6);
    }

    public function setPromo(){
        try{
            Authentication::checkAccessRight(2);
            $p = $this->request->post["items"];
            $list = Item::where('promo', '=', '1')->get();
            if (isset($list)) {
                foreach($list as $v){
                    $v->promo = 0;
                    $v->save();
                }
            }
            if (isset($p)){
                foreach( $p as $v ){
                    $i = Item::find($v);
                    $i->promo = 1;
                    $i->save();
                }
            }
            $v = new VueAdmin($this->request);
            $v->render(6);
        } catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, $e->getMessage());
        }
    }

    /**
     * methode qui creer la vue pour ajouter un utilisateur
     */
    public function addUser(){
        $v = new VueAdmin($this->request);
        $v->render(4);
    }

    /**
     * methode qui enregistre un nouvel utilisateur dans la base de donnees
     */
    public function saveUser(){
        try {
            if (isset($this->request->post["pseudo"]) && isset($this->request->post["password"]) && isset($this->request->post["password2"])) {
                if ($this->request->post["password"] == $this->request->post["password2"]) {
                    Authentication::createUser($this->request->post["pseudo"], $this->request->post["password"], 1);
                    $list = Item::all();
                    $v = new VueCatalogue($list, $this->request);
                    $v->render(2);
                }
            } else {
                self::addUser();
            }
        }catch (QueryException $e){
            $v = new VueAdmin($this->request, "le pseudo est déjà utilisé");
            $v->render(4);
        } catch (AuthException $e){
            $v = new VueAdmin($this->request, $e->getMessage());
            $v->render(4);
        }
    }

    /**
     * methode qui creer la vue pour se loguer
     */
    public function login(){
        $v = new VueAdmin($this->request);
        $v->render(3);
    }

    /**
     * methode qui lance authentification d'un utilisateur
     */
    public function authentify(){
        try {
            if (isset($this->request->post["pseudo"]) && isset($this->request->post["password"])) {
                Authentication::authenticate($this->request->post["pseudo"], $this->request->post["password"]);
                $list = Item::all();
                $v = new VueCatalogue($list, $this->request);
                $v->render(2);
            } else {
                self::login();
            }
        }catch (AuthException $e){
            $v = new VueAdmin($this->request, $e->getMessage());
            $v->render(3);
        }
    }

    /**
     * methode de deconnexion
     */
    public function logout(){
        session_destroy();
        session_start();
        $list = Item::all();
        $v = new VueCatalogue($list, $this->request);
        $v->render(2);
    }

    public function addCommentaire()
    {
        try {
            Authentication::checkAccessRight(1);
            if(isset($_GET['id'])){
                $_SESSION['item'] = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
                $vue = new VueAdmin($this->request);
                $vue -> render(7);
            }           
        } catch(AuthException $e){
             $v = new VueCatalogue(null, $this->request);
            $v->render(5, $e->getMessage());
        }
        
    }

    public function saveCommentaire(){
        try {
            Authentication::checkAccessRight(1);
            if (isset($this->request->post["texte"])) {
                $c = new Commentaire();
                $c->texte = htmlspecialchars($this->request->post["texte"]);
                $c->item_id = $_SESSION['item'];
                $u = $_SESSION['user'];
                $c->user_id = $u['userid'];
                $c->save();
                header("Location: ".dirname($this->request->script_name)."/catalogue/item?id=".$_SESSION['item']);
            } else {
                self::addType();
            }
        } catch (AuthException $e){
            $v = new VueCatalogue(null, $this->request);
            $v->render(5, $e->getMessage());
        }
    }
}