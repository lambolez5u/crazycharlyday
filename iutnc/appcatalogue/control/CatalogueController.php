<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 19/11/14
 * Time: 08:59
 */

namespace iutnc\appcatalogue\control;

use iutnc\appcatalogue\models\Type;
use iutnc\appcatalogue\models\Piece;
use iutnc\picof\AbstractController;
use iutnc\appcatalogue\models\Item;
use iutnc\appcatalogue\vue\VueCatalogue;


/**
 * Class BlogController
 * @package iutnc_madert2u\blogapp\control
 */
class CatalogueController extends AbstractController {

    /**
     * methode qui creer la vue pour afficher la liste des items
     */
    public function listItems(){
        $list = Item::select()->get();
        $v = new VueCatalogue($list, $this->request);
        $v->render(2);
    }

    /**
     * methode qui creer la vue pour afficher un items
     */
    public function afficheItem(){
        if ($this->request->get != null){
            $i = $this->request->get["id"];
            if (is_numeric($i)){
                $b = Item::find($i);
                if ($b !=  null){
                    $v = new VueCatalogue($b, $this->request);
                    $v->render(1);
                } else {
                    $v = new VueCatalogue(null, $this->request);
                    $v->render(100, "not find...");
                }
            } else {
                $v = new VueCatalogue(null, $this->request);
                $v->render(100, "invalid request !");
            }
        } else {
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, "param absent !");
        }
    }

    /**
     * methode qui creer la vue pour afficher la liste des categories
     */
    public function listTypes(){
        /*$c = Categorie::all();
        $v = new VueBlog($c, $this->request);
        $v->render(3);*/
    }

    /**
     * methode qui creer la vue pour afficher la liste des items pour un type
     */
    public function listItemsType()
    {
       if ($this->request->get != null) {
            $i = $this->request->get["id"];
            if (is_numeric($i)) {
                $c = Type::find($i);
                $items = $c->items;
                if ($c != null) {
                    $v = new VueCatalogue($items, $this->request);
                    $v->render(2);
                } else {
                    $v = new VueCatalogue(null, $this->request);
                    $v->render(100, "not find...");
                }
            } else {
                $v = new VueCatalogue(null, $this->request);
                $v->render(100, "invalid request !");
            }
        } else {
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, "param absent !");
        }
    }

    /**
     * methode qui creer la vue pour afficher la liste des categories
     */
    public function listPieces(){
        /*$c = Categorie::all();
        $v = new VueBlog($c, $this->request);
        $v->render(3);*/
    }

    /**
     * methode qui creer la vue pour afficher la liste des billets pour une categorie
     */
    public function listItemsPiece()
    {
        if ($this->request->get != null) {
            $i = $this->request->get["id"];
            if (is_numeric($i)) {
                $c = Piece::find($i);
                $items = $c->items;
                if ($c != null) {
                    $v = new VueCatalogue($items, $this->request);
                    $v->render(2);
                } else {
                    $v = new VueCatalogue(null, $this->request);
                    $v->render(100, "not find...");
                }
            } else {
                $v = new VueCatalogue(null, $this->request);
                $v->render(100, "invalid request !");
            }
        } else {
            $v = new VueCatalogue(null, $this->request);
            $v->render(100, "param absent !");
        }
    }

    public function listItemsCouleur(){
        if($this->request->get != null){
            $i = filter_var($this->request->get["c"],FILTER_SANITIZE_STRING);
            $items = Item::where('couleur','=',$i)->get();
            if(count($items) > 0){
                $v = new VueCatalogue($items, $this->request);
                $v->render(2);
            }else{
                $v = new VueCatalogue(null, $this->request);
                $v->render(100, "invalid request !");
            }
        }
    }
} 