<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 16/12/14
 * Time: 10:12
 */

namespace iutnc\appcatalogue\vue;


use iutnc\appcatalogue\models\Item;
use iutnc\appcatalogue\models\Type;
use iutnc\picof\utils\HttpRequest;

/**
 * Class VueCatalogue
 * @package iutnc\appcatalogue\vue
 */
class VueCatalogue {

    private $b, $request, $s;

    /**
     * constructeur de la vue du catalogue
     * @param $b : liste d'Items
     * @param HttpRequest $http
     */
    public function __construct($b,HttpRequest $http){
        $this->b = $b;
        $this->request = $http;
        $this->s = dirname($this->request->script_name);
    }

    /**
     * methode qui affiche une page en fonction de la commande
     * @param $i : commande
     */
    public function render($i, $e=null){
        $css = dirname($this->request->script_name);
        //$this->affListTypes();
        $isconnect = "";
        if(isset($_SESSION['user'])){
            $isconnect = "<a href=\"$css/admin/user/logout\" id=\"\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-check\">Deconnexion</span></a>";
            if ($_SESSION['user']['droit']>=2)
                $isconnect.="<li><a href='$this->s/admin/item/add' id='contact-link' class='skel-layers-ignoreHref'><span class='icon fa-th-list'>ajouter item</span></a></li>";
        }else{
            $isconnect = "<a href=\"$css/admin/user/login\" id=\"\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-check\">Connexion</span></a>";
        }

        $promo = $this->affPromo();

        switch($i){
            case 1 :
                $b = $this->aff1Item();
                break;
            case 2 :
                $b = $this->affListItems();
                break;
            case 3 :
                $b = $this->affFiltreType();
                break;
            case 4 :
                $b = $this->affFiltrePiece();
                break;
            case 5 :
               // $b = $this->affFiltreCouleur();
            case 100 ;
                $b = $this->affErreur($e);
        }
        //$c = $this->affFiltreType();
        
        //$b10 = $this->aff1OItems();
        echo 
<<<END
<html>
    <head>
        <title>LC2M</title>
        <meta charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="$css/web/js/jquery.min.js"></script>
        <script src="$css/web/js/jquery.scrolly.min.js"></script>
        <script src="$css/web/js/jquery.scrollzer.min.js"></script>
        <script src="$css/web/js/skel.min.js"></script>
        <script src="$css/web/js/skel-layers.min.js"></script>
        <script src="$css/web/js/init.js"></script>
            <link rel="stylesheet" href="$css/web/css/skel.css" />
            <link rel="stylesheet" href="$css/web/css/style.css" />
            <link rel="stylesheet" href="$css/web/css/style-wide.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="$css/web/css/ie/v9.css" /><![endif]-->
        <!--[if lte IE 8]><link rel="stylesheet" href="$css/web/css/ie/v8.css" /><![endif]-->
    </head>
    <body>

        <!-- Header -->
            <div id="header" class="skel-layers-fixed">

                <div class="top">

                    <!-- Logo -->
                        <div id="logo">
                            <span class="image avatar48"><img src="$css/web/images/avatar1.png" alt="" /></span>
                            <h1 id="title">LC2M</h1>
                            <p>Recyclable Commerce</p>
                        </div>

                    <!-- Nav -->
                        <nav id="nav">
                            <!--
                            
                                Prologue's nav expects links in one of two formats:
                                
                                1. Hash link (scrolls to a different section within the page)
                                
                                   <li><a href="#foobar" id="foobar-link" class="icon fa-whatever-icon-you-want skel-layers-ignoreHref"><span class="label">Foobar</span></a></li>

                                2. Standard link (sends the user to another page/site)

                                   <li><a href="http://foobar.tld" id="foobar-link" class="icon fa-whatever-icon-you-want"><span class="label">Foobar</span></a></li>
                            
                            -->
                            <ul>
                                <li><a href="$css/" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Intro</span></a></li>
                                <li><a href="$css/admin/user/add" id="" class="skel-layers-ignoreHref"><span class="icon fa-user">S'inscrire</span></a></li>
                                <li>$isconnect</li>
                                <li><a href="#portfolio" id="portfolio-link" class="skel-layers-ignoreHref"><span class="icon fa-th">Nos Promos</span></a></li>
                                <li><a href="$css/#contact" id="contact-link" class="skel-layers-ignoreHref"><span class="icon fa-th-list">Catalogue</span></a></li>
                            </ul>
                        </nav>
                        
                </div>
                
                <div class="bottom">

                    <!-- Social Icons -->
                        <ul class="icons">
                            <li><a class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                            <li><a class="icon fa-envelope"><span class="label">Email</span></a></li>
                        </ul>
                
                </div>
            
            </div>

        <!-- Main -->
            <div id="main">

                <!-- Intro -->
                    <section id="top" class="one dark cover">
                        <div class="container">

                            <header>
                                <h2 class="alt">Bonjour nous sommes <strong>LC2M</strong>, une société écologique<br /></h2>
                                <p>Nos produits sont<br />
                                cértifiés <strong>100%</strong> recyclés</p>
                            </header>
                            
                            <footer>
                                <a href="#contact" class="button scrolly">Notre Catalogue</a>
                            </footer>

                        </div>
                    </section>

                    <!-- About Me -->
                    <section id="portfolio" class="three">
                        <div class="container">

                            <header>
                                <h2>Les Promos du moment !</h2>
                            </header>
                            $promo

                        </div>
                    </section>
                    $b
                    <!-- Footer -->
            <div id="footer">
                
                <!-- Copyright -->
                    <ul class="copyright">
                        <li>&copy; Untitled. All rights reserved.</li><li>LAMBOLEZ Théodore, MADERT Carl, MOREL Rémi, COMTE Clément</a></li>
                    </ul>
                
            </div>
    </body>
</html>
END;

    }


    private function affPromo(){
        $b = Item::where('promo', '=', '1')->get();
        $s = "<div class=\"row\">";
        foreach ($b as $v) {
            $s .= "<div class=\"4u\">
                                    <article class=\"item\">
                                        <a href=\"$this->s/catalogue/item?id=$v->id\" class=\"image fit\"><img src=\"$this->s/images/$v->photo\"/></a>
                                        <header>
                                            <h3>$v->nom - $v->prix €</h3></br>
                                        </header>
                                    </article></div>";
        }
        return $s."</div>";
    }

    /**
     * methode qui affiche un seul billet
     * @return string
     */
    private function aff1Item(){
        $v = $this->b;
        $chaine = "<div class=\"4u\">
                                    <article class=\"item\">
                                        <a href=\"$this->s/catalogue/item?id=$v->id\" class=\"image fit\"><img src=\"$this->s/images/$v->photo\"/></a>
                                        <header><h3>".$this->b->nom."<br/>".$this->b->prix."€<br/>".$this->b->couleur."<br/></h3></article>";
        $chaine = $chaine."<div class=\"like-btn\">J'aime</div>"."<br/><a href=\"".$this->s."/admin/comm/add?id=".$this->b->id."\">Ajouter Un Commentaire</a></div><br/>";
        $list_comm = $this->b->commentaires;
        foreach ($list_comm as $c) {
            $chaine = $chaine.$c->texte."<br/>";
        }
        return $chaine."</div>";
    }

    /**
     * methode qui affiche la liste des billet
     * @return string
     */
    private function affListItems(){
        $r = "<section id=\"contact\" class=\"four\">
                        <div class=\"container\"><header>
                <h2>Catalogue</h2>
            </header>";
        foreach($this->b as $v) {
            $r .= "<div class=\"4u\"><article class=\"item\"><a href=\"$this->s/catalogue/item?id=$v->id\" class=\"image fit\"><img src=\"$this->s/images/".$v->photo."\" alt=\"\" /></a>
                                        <header>
                                            <h3>$v->nom - $v->prix €</h3>
                                            </header>
                                    </article></div>";
        }
        return $r."</div>
                    </section>";
    }

    /**
     * methode qui affiche la zone avec le lien vers l'accueil, la liste des categories,
     * la zone de connection et de gestion du blog
     * @return string
     */
    private function affFiltreType(){
        return "<div>".$this->b->type."</div>";
    }

    /**
     * methode qui affiche la liste des 10 derniers billets saisis
     * @return string
     */
 /*   private function aff1OItems(){
        $b = Billet::select('*')->orderBy('date', 'desc')->take(10)->get();
        $r = "<div class='dixBillets'><h3>les 10 dernières news :</h3><ul>";
        foreach($b as $v){
            $r.= "<li><a href=\"".$this->s."/blog/billet?id=".$v->id."\">".$v->titre."</a></li>\n";
        }
        $r.="</ul></div>";
        return $r;
    }





    /**
     * methode qui affiche la liste des categories et leur description
     * @return string
     */
    private function affListTypes(){
        $r = "<article><ul>";
        foreach($this->b as $v) {
            $r .= "<li><a href=\"" . $this->s . "/blog/cat?id=" . $v->id . "\">" . $v->titre . "</a><br/>\n<p>".$v->description."</p></li>";
        }
        $r .= "</ul></article>";
        return $r;
    }

    /**
     * methode qui affiche la liste des billets pour une categorie
     * @return string
     */
    public function affListItemType(){
        $r = "<article><h3>" . $this->b->titre . '</h3><p>' . $this->b->description . '</p><ul>';
        $list = $this->b->billets;
        foreach($list as $v){
            $r.= "<li><a href=\"".$this->s."/blog/billet?id=".$v->id."\">".$v->titre."</a></li>";
        }
        $r .= "</ul></article>";
        return $r;
    }

    public function affErreur($e){
        return <<<END
<article><p>$e</p></article>
END;

    }
}