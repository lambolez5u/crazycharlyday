<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 19/12/14
 * Time: 08:43
 */

namespace iutnc\appcatalogue\vue;


use iutnc\appcatalogue\models\Categorie;
use iutnc\appcatalogue\models\User;
use iutnc\picof\utils\HttpRequest;
use iutnc\appcatalogue\models\Item;
use iutnc\appcatalogue\models\Type;
use iutnc\appcatalogue\models\Piece;

/**
 * Class VueAdmin
 * @package iutnc_madert2u\blogapp\vue
 */
class VueAdmin {

    private $request, $erreur, $s;

    /**
     * constructeur de la vue administateur
     * @param HttpRequest $http
     */
    public function __construct(HttpRequest $http, $err=null){
        $this->request = $http;
        $this->erreur = $err;
        $this->s = dirname($this->request->script_name);
    }

    /**
     * fonction qui affiche la page en fonction de la commande
     * @param $i : commande
     */
    public function render($i){
        $css = dirname($this->request->script_name);
        switch($i){
            case 1 :
                $b = $this->affAddItem();
                break;
            case 2 :
                $b = $this->affAddType();
                break;
            case 3 :
                $b = $this->affLogin();
                break;
            case 4 :
                $b = $this->affAddUser();
                break;
            case 5 :
                $b = $this->affAddPiece();
                break;
            case 6 :
                $b = $this->affSetPromo();
                break;
            case 7 :
                $b = $this->affAddCom();
        }
        
        echo <<<END
<html>
    <head>
        <title>LC2M</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="$css/web/js/jquery.min.js"></script>
        <script src="$css/web/js/jquery.scrolly.min.js"></script>
        <script src="$css/web/js/jquery.scrollzer.min.js"></script>
        <script src="$css/web/js/skel.min.js"></script>
        <script src="$css/web/js/skel-layers.min.js"></script>
        <script src="$css/web/js/init.js"></script>
            <link rel="stylesheet" href="$css/web/css/skel.css" />
            <link rel="stylesheet" href="$css/web/css/style.css" />
            <link rel="stylesheet" href="$css/web/css/style-wide.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="$css/web/css/ie/v9.css" /><![endif]-->
        <!--[if lte IE 8]><link rel="stylesheet" href="$css/web/css/ie/v8.css" /><![endif]-->
    </head>
    <body>

        <!-- Header -->
            <div id="header" class="skel-layers-fixed">

                <div class="top">

                    <!-- Logo -->
                        <div id="logo">
                            <span class="image avatar48"><img src="$css/web/images/avatar1.png" alt="" /></span>
                            <h1 id="title">LC2M</h1>
                            <p>Recyclable Commerce</p>
                        </div>

                    <!-- Nav -->
                        <nav id="nav">
                            <!--
                            
                                Prologue's nav expects links in one of two formats:
                                
                                1. Hash link (scrolls to a different section within the page)
                                
                                   <li><a href="#foobar" id="foobar-link" class="icon fa-whatever-icon-you-want skel-layers-ignoreHref"><span class="label">Foobar</span></a></li>

                                2. Standard link (sends the user to another page/site)

                                   <li><a href="http://foobar.tld" id="foobar-link" class="icon fa-whatever-icon-you-want"><span class="label">Foobar</span></a></li>
                            
                            -->
                            <ul>
                                <li><a href="index.html" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Menu</span></a></li>
                                <li><a href="$this->s/admin/user/add" id="about-link" class="skel-layers-ignoreHref"><span class="icon fa-user">S'inscrire</span></a></li>
                            </ul>
                        </nav>
                        
                </div>
                
                <div class="bottom">

                    <!-- Social Icons -->
                        <ul class="icons">
                            <li><a class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                            <li><a class="icon fa-envelope"><span class="label">Email</span></a></li>
                        </ul>
                
                </div>
            
            </div>
            $b
            <!-- Footer -->
            <div id="footer">
                
                <!-- Copyright -->
                    <ul class="copyright">
                        <li>&copy; Untitled. All rights reserved.</li><li>LAMBOLEZ Théodore, MADERT Carl, MOREL Rémi, COMTE Clément</a></li>
                    </ul>
                
            </div>

    </body>
</html>
END;

    }

    /**
     * methode qui affiche le formulaire pour ajouter un item
     * @return string
     */
    private function affAddItem(){
        $c = Type::all();
        $d = Piece::all();
        $l = "";
        $x = "";
        $e = "";
        if ($this->erreur != null){
            $e = $this->erreur;
        }
        foreach ($c as $v){
            $l .= "            <option value=".$v->id.">".$v->type."</option>\n";
        }
        foreach ($d as $w){
            $x .= "            <option value=".$w->id.">".$w->nom."</option>\n";
        }
        return <<<END
        <div id="main">

                <!-- Intro -->
                    <section id="top" class="one dark cover">
                        <div class="container">

                            <header>
                                <h2 class="alt">Nouvel <strong>Item</strong></h2>
                                
                            </header>
                            <section id=\"contact\" class=\"four\">
                            <div class=\"container\">
                            <form method='post' action='$this->s/admin/item/save'>
                            <input type='text' name='nom' placeholder='nom' autofocus='' required /><br/>
                            <label for="type">type :</label>
                             <select name="type" id="type">
                                $l
                             </select>
                                <a href='$this->s/admin/type/add'>ajouter un type</a><br/>
                                 <label for="piece">piece :</label>
                                <select name="piece" id="piece">
                                    $x
                                </select>
                                <a href='$this->s/admin/piece/add'>ajouter une piece</a><br/>
                        <input type='text' name='couleur' placeholder='couleur' autofocus='' required /><br/>
                        <input type='number' name='prix' placeholder='prix' autofocus='' required /><br/>
                         <textarea name='description' placeholder='saisir votre texte ici'></textarea><br/>
                        <div class='erreur'>$e</div>
                        <br/>
                        <button type='submit' name='valider'>Valider</button>
                            </form>
                            </div>
                            </section>

                        </div>
                    </section>


        

    </body>
    </html>
END;
    }

    /**
     * methode qui affiche le formulaire pour ajouter une categorie
     * @return string
     */
    private function affAddType(){
        return <<<END
 <div id="main">

                <!-- Intro -->
                    <section id="top" class="one dark cover">
                        <div class="container">
<form method='post' action='$this->s/admin/type/save'>
        <input type='text' name='nom' placeholder='nom du nouveau type' autofocus='' required /><br/>
        <br/>
        <button type='submit' name='valider'>Valider</button>
    </form>
    </div>
    </section>
</div>
END;
    }

    private function affAddPiece(){
        return <<<END
 <div id="main">

                <!-- Intro -->
                    <section id="top" class="one dark cover">
                        <div class="container">
<form method='post' action='$this->s/admin/piece/save'>
        <input type='text' name='nom' placeholder='nom de la nouvelle piece' autofocus='' required /><br/>
        <textarea name='description' placeholder='saisir votre texte ici'></textarea><br/>
        <br/>
        <button type='submit' name='valider'>Valider</button>
    </form>
    </div>
    </section>
</div>
END;
    }

    /**
     * methode qui affiche le formulaire pour creer un utilisateur
     * @return string
     */
    private function affAddUser(){
        $e = "";
        if ($this->erreur!=null){
            $e = $this->erreur;
        }
        return <<<END
 <div id="main">

                <!-- Intro -->
                    <section id="top" class="one dark cover">
                        <div class="container">
<form method='post' action='$this->s/admin/user/save'>
        <input type='text' name='pseudo' placeholder='pseudo' autofocus='' required /><br/>
        <input type='password' name='password' placeholder='mot de passe' required /><br/>
        <input type='password' name='password2' placeholder='repeter le mot de passe' required /><br/>
        <br/>
        <div class='erreur'>$e</div><br/>
        <button type='submit' name='valider'>Valider</button>
    </form>
    </div>
</section>
</div>
END;
    }

    /**
     * methode qui affiche le formulaire pour se connecter
     * @return string
     */
    private function affLogin(){
        $e = "";
        if ($this->erreur!=null){
            $e = $this->erreur;
        }
        return <<<END
<div id="main">

                <!-- Intro -->
                    <section id="top" class="one dark cover">
                        <div class="container">

                            <header>
                                <h2 class="alt">Connectez <strong>vous</strong></h2>
                                
                            </header>
                            <section id=\"contact\" class=\"four\">
                            <div class=\"container\">
<form method='post' action='$this->s/admin/user/auth'>
        <input type='text' name='pseudo' placeholder='pseudo' autofocus='' required /><br/>
        <input type='password' name='password' placeholder='password' required /><br/>
        <br/>
        <div class='erreur'>$e</div><br/>
        <button type='submit' name='valider'>Valider</button><br/>
        <a href='$this->s/admin/user/add'>incrivez vous</a>
    </form>
    </div>
                            </section>

                        </div>
                    </section>


        

    </body>
    </html>
END;
    }

    private function affSetPromo()
    {
        $i = Item::all();
        $s = "
        <div class='admin'>
        <form method='post' action='$this->s/admin/item/setpromo'>
";
        foreach ($i as $v) {
            if ($v->promo==1){
                $check = " checked=\"checked\"";
            } else {
                $check = "";
            }
            $s .= "<input type='checkbox' name='items[]' value=$v->id".$check.">$v->nom</input>\n";
        }
        $s .= "<button type='submit' name='valider'>Valider</button></form></div>";
        return $s;
    }

    private function affAddCom(){
        $e = "";
        if ($this->erreur!=null){
            $e = $this->erreur;
        }
        return <<<END
 <div id="main">

                <!-- Intro -->
                    <section id="top" class="one dark cover">
                        <div class="container">
<form method='post' action='$this->s/admin/comm/save'>
       <textarea name='texte' placeholder='saisir votre texte ici'></textarea><br/><br/>
        <div class='erreur'>$e</div><br/>
        <button type='submit' name='valider'>Valider</button>
    </form>
    </div
    </section>
    </div>
END;
    }
}