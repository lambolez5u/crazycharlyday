<?php
/**
 * Created by PhpStorm.
 * User: carl
 * Date: 18/11/14
 * Time: 21:14
 */

// demarrage de la session
session_start();

require_once 'vendor/autoload.php';

use iutnc\picof\utils\HttpRequest;
use iutnc\picof\dispatch\Dispatcher;

$http = new HttpRequest();

\conf\ConfigEloquent::configurer();

// creation du dispatcher et des routes
$d = new Dispatcher($http);
$d->addRoute("/", '\iutnc\appcatalogue\control\CatalogueController', "listItems");
$d->addRoute("/catalogue/list", '\iutnc\appcatalogue\control\CatalogueController', "listItems");
$d->addRoute("/catalogue/item", '\iutnc\appcatalogue\control\CatalogueController', 'afficheItem');
$d->addRoute("/catalogue/type", '\iutnc\appcatalogue\control\CatalogueController', 'listItemsType');
$d->addRoute("/catalogue/piece", '\iutnc\appcatalogue\control\CatalogueController', 'listItemsPiece');
$d->addRoute("/catalogue/color", '\iutnc\appcatalogue\control\CatalogueController', 'listItemsCouleur');
$d->addRoute("/admin/item/add", '\iutnc\appcatalogue\control\AdminController', 'addItem');
$d->addRoute("/admin/item/save", '\iutnc\appcatalogue\control\AdminController', 'saveItem');
$d->addRoute("/admin/item/setpromo", '\iutnc\appcatalogue\control\AdminController', 'setPromo');
$d->addRoute("/admin/item/affsetpromo", '\iutnc\appcatalogue\control\AdminController', 'affSetPromo');
$d->addRoute("/admin/type/add", '\iutnc\appcatalogue\control\AdminController', 'addType');
$d->addRoute("/admin/type/save", '\iutnc\appcatalogue\control\AdminController', 'saveType');
$d->addRoute("/admin/piece/add", '\iutnc\appcatalogue\control\AdminController', 'addPiece');
$d->addRoute("/admin/piece/save", '\iutnc\appcatalogue\control\AdminController', 'savePiece');
$d->addRoute("/admin/comm/add", '\iutnc\appcatalogue\control\AdminController', 'addCommentaire');
$d->addRoute("/admin/comm/save", '\iutnc\appcatalogue\control\AdminController', 'saveCommentaire');
$d->addRoute("/admin/user/add", '\iutnc\appcatalogue\control\AdminController', 'addUser');
$d->addRoute("/admin/user/save", '\iutnc\appcatalogue\control\AdminController', 'saveUser');
$d->addRoute("/admin/user/login", '\iutnc\appcatalogue\control\AdminController', 'login');
$d->addRoute("/admin/user/auth", '\iutnc\appcatalogue\control\AdminController', 'authentify');
$d->addRoute("/admin/user/logout", '\iutnc\appcatalogue\control\AdminController', 'logout');

// lance la commande
$d->dispatch();
